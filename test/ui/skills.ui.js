(() => {


    function packIntervals() {
        const testSkills = [
            {
                "name": "Perl",
                "category": "techno",
                "date_start": "3/1/2000",
                "date_mode": "6/1/2011",
                "date_end": "8/22/2015",
                "level": 4,
                "description_length": 37,
                "description": "... And I live with a cat named Perl.",
                "links": "http://bit.ly/cicd-perl;Continuous Deployment in Perl: Code & Folks"
            }, {
                "name": "Akka",
                "category": "techno",
                "date_start": "6/1/2012",
                "date_mode": "6/1/2013",
                "date_end": "8/22/2019",
                "level": 2,
                "description_length": null,
                "description": null,
                "links": null
            }, {
                "name": "ukulele",
                "category": "techno",
                "date_start": "6/1/2002",
                "date_mode": null,
                "date_end": "1/1/2006",
                "level": 3,
                "description_length": null,
                "description": null,
                "links": null
            }, {
                "name": "Scala",
                "category": "techno",
                "date_start": "6/1/2012",
                "date_mode": "12/1/2016",
                "date_end": "8/22/2018",
                "level": 4,
                "description_length": null,
                "description": null,
                "links": null
            },
            {
                "name": "Groovy",
                "category": "techno",
                "date_start": "6/1/2008",
                "date_mode": "12/1/2011",
                "date_end": "12/1/2011",
                "level": 2,
                "description_length": null,
                "description": null,
                "links": null
            }
        ];

        const builder = new SkillTimelinetBuilder();
        const mashedSkills = builder.build(testSkills, {
            techno:
                {
                    rank: 5,
                    display: 'Technologies'
                }
        })['techno'];
        const packer = new IntervalPacker(
            (s) => s.date_start.valueOf(),
            (s) => s.date_end.valueOf(),
        );
        return packer.pack(mashedSkills, 'topFirst')
    }
    const packedSkills = packIntervals();
    const renderer = new PackedSkillsRenderer();

    renderer.displaySkills(
        document.getElementById("skills-1"),
        packedSkills
    );
    renderer.displaySkills(
        document.getElementById("skills-2"),
        packedSkills
    );


    renderer.displayCategoryTitle(
        document.getElementById("category-1"),
        ['Technos']
    );


    renderer.displayCategories(document.getElementById('category-list-1'),
        [
            {display: 'a very \nlong one (50)', color: 'red'},
            {display: 'short (50)', color: 'green'},
            {display: 'kinda medium (80)', color: 'blue'}
        ],
        [
            {height: 50},
            {height: 50},
            {height: 80}
        ]
    );

    renderer.displayCategories(document.getElementById('category-list-2'),
        [
            {display: 'a single very\nlong one with \\n\n(height bounded)', color: 'red'}
        ],
        [
            {height: 180}
        ]
    );
    renderer.displayCategories(document.getElementById('category-list-3'),
        [
            {display: 'a single very long one'}
        ],
        [
            {height: 180}
        ]
    );

    const positions = [
        {
            name: 'Rosney Inc.',
            location: 'Los Angeles',
            nb_employees: '25\'000',
            role: 'Janitor',
            date_start: new Date('3/1/1995'),
            date_end: new Date('3/1/2003'),
            description: 'I was in charge of making sure everything was working there',
            is_starting_before: true
        },
        {
            name: 'Super Bingo',
            location: 'Montcul',
            nb_employees: '5',
            role: 'head of pipo',
            date_start: new Date('3/1/2003'),
            date_end: new Date('12/1/2003'),
            description: 'I was supposed to be the boss, but that was a full mess',
            is_failure: true
        },
        {
            name: 'Bixar',
            location: 'San Bernardino',
            nb_employees: '2\'000',
            role: 'Chief Pipeau Officer',
            date_start: new Date('12/1/2003'),
            date_end: new Date('3/28/2012'),
            description: 'Anything creative came out of my mind...'
        },
        {
            name: 'Epicerie Truc',
            location: 'Gingins',
            nb_employees: '2',
            role: 'Cashier',
            date_start: new Date('4/1/2012'),
            date_end: new Date('6/30/2018'),
            description: 'Serving cheese, with pride',
            is_ongoing: true
        },
    ];
    renderer.displayPositions(document.getElementById('positions-1'),
        positions,
        {height: 50}
    );
    renderer.displayPositions(document.getElementById('positions-2'),
        positions,
        {height: 50}
    );


    renderer.displayXAxis(document.getElementById('xaxis-1'),
        new Date('3/1/1995'),
        new Date('6/30/2018')
    );
    renderer.displayXAxis(document.getElementById('xaxis-2'),
        new Date('3/1/1995'),
        new Date('6/30/2018')
    );


    const lorem = new Lorem();
    const defaultSkill =
        {
            "name": lorem.createText(2, Lorem.TYPE.WORD),
            "category": "techno",
            "date_start": "3/1/2000",
            "date_mode": "6/1/2011",
            "date_end": "8/22/2015",
            "level": 4,
            "description": lorem.createText(25, Lorem.TYPE.WORD),
            "links": "http://bit.ly/cicd-perl;Continuous Deployment in Perl: Code & Folks\nhttp://ducon.com;relax"
        };
    const elOneSkill = d3.select(document.getElementById('skill-detail-widget'));
    const detailData = [
        {
            anchor: [0, 25],
            skill: {...defaultSkill}
        },
        {
            anchor: [399, 25],
            skill: {...defaultSkill, name: lorem.createText(4, Lorem.TYPE.WORD)}
        },
        {
            anchor: [799, 25],
            skill: {...defaultSkill, links: undefined}
        },
        {
            anchor: [0, 250],
            skill: {...defaultSkill, links: '   http://bit.ly/cicd-perl;Continuous Deployment in Perl: Code & Folks'}
        },
        {
            anchor: [430, 250],
            skill: {...defaultSkill, description: lorem.createText(12, Lorem.TYPE.WORD)}
        },
        {
            anchor: [750, 260],
            skill: {...defaultSkill}
        },
        {
            anchor: [0, 500],
            skill: {...defaultSkill}
        },
        {
            anchor: [380, 500],
            skill: {...defaultSkill, description: lorem.createText(12, Lorem.TYPE.WORD)}
        },
        {
            anchor: [800, 500],
            skill: {...defaultSkill}
        }
    ];
    detailData.forEach((d) => d.skill = new SkillTimelinetBuilder().mashSkill(d.skill, {
        techno:
            {
                rank: 5,
                display: 'Technologies'
            }
    }));

    const elAnchors = elOneSkill
        .selectAll('circle')
        .data(detailData)
        .enter()
        .append('circle')
        .attr('cx', c => c.anchor[0])
        .attr('cy', c => c.anchor[1])
        .attr('r', 20)
        .style('fill', 'orangered')
        .style('opacity', 0.6);

    elAnchors.each(function (pSkill) {
            const sdw = new ModalDetails(
                elOneSkill,
                {width: 200, height: 100}
            );
            sdw.show(
                d3.select(this),
                pSkill.skill
            );
            d3.select(this)
                .on('mouseenter', () => sdw.show(
                    d3.select(this),
                    pSkill.skill
                    )
                )
        }
    );


})();