// const positions =
//     [
//         {"date_start":"6/1/1996","date_end":"3/1/2000","organization":"University of Geneva","role":"Master (Maths & CS) - Ph.D. (CS)","number_of_employees":"","is_failure":"","is_started_before":"","is_on_going":"","description":""},
//         {"date_start":"3/1/2000","date_end":"5/1/2003","organization":"GeneProt","role":"Bioinformatician","number_of_employees":"","is_failure":"","is_started_before":"","is_on_going":"","description":""},
//         {"date_start":"5/1/2003","date_end":"9/1/2011","organization":"GeneBio","role":"CTO","number_of_employees":"","is_failure":"","is_started_before":"","is_on_going":"","description":""},
//         {"date_start":"9/1/2011","date_end":"3/1/2012","organization":"Heath On the Net","role":"Tech lead","number_of_employees":"","is_failure":"1","is_started_before":"","is_on_going":"","description":""},
//         {"date_start":"3/1/2012","date_end":"11/1/2014","organization":"Genentech","role":"Sr. computational biologist","number_of_employees":"","is_failure":"","is_started_before":"","is_on_going":"","description":""},
//         {"date_start":"11/1/2014","date_end":"9/1/2015","organization":"Vital-IT","role":"N/A","number_of_employees":"","is_failure":"1","is_started_before":"","is_on_going":"","description":"promise vs. reality. Can drive change without top management support"},
//         {"date_start":"9/1/2015","date_end":"10/28/2018","organization":"OCTO Technology Switzerland","role":"Senior manager & consultant","number_of_employees":"20","is_failure":"","is_started_before":"","is_on_going":"1","description":"architect"}
//     ]
//
// ;

const skillsMeta = {
    'management/lead':
        {
            rank: 0,
            display: 'Management\n& Leadership',
            color: '#66c2a5'
        },
    'methodo':
        {
            rank: 1,
            display: 'Methodology',
            color: '#fc8d62'
        },
    'teaching':
        {
            rank: 2,
            display: 'Teaching',
            color: '#8da0cb'
        },
    'domain':
        {
            rank: 3,
            display: 'Domains',
            color: '#e78ac3'
        },
    'data viz':
        {
            rank: 4,
            display: 'Data\nviz',
            color: '#a6d854'
        },
    'techno':
        {
            rank: 5,
            display: 'Technologies',
            color: '#FFC107'
        }
};

//const skillsRaw = d3.csv('src/data/cv topic - skills.csv')
    // [
    //     {"name":"high performance","category":"data viz","date_start":"6/1/2012","date_mode":"6/1/2014","date_end":"10/30/2018","level":"4","description_length":"218","description":"There are so many pixels on your screen, so much width on you band ans so much information one brain can process. Various techniques, both on the back and front ends, must be put in place to handle large aount of data.","links":""},
    //     {"name":"Dabshboards","category":"data viz","date_start":"3/1/2016","date_mode":"6/1/2017","date_end":"12/1/2017","level":"4","description_length":"229","description":"Between fully custom visualization (JavaScript/d3) and static reports (R/knitr) lies a series of powerful dasbhoarding tools.If I had most development experience with Kibana, Tableau and Grafana are definitely part of my tool box","links":"https://portal.klewel.com/watch/webcast/elastic-search-march-2017/talk/2,Building and deploying Kibana plugins ... And should I do it?;\nhttps://blog.octo.com/en/a-journey-into-industrializing-the-writing-and-deployment-of-kibana-plugins-riding-docker/,A Journey into Industrializing the Writing and Deployment of Kibana Plugins (riding Docker)"},
    //     {"name":"physics models","category":"data viz","date_start":"9/1/1992","date_mode":"3/1/2000","date_end":"3/1/2000","level":"4","description_length":"232","description":"My Ph.D. period exposed me to various numerical model, and therefore a fertile ground for visualization. I could apply a vast range of techniques, from realtime rendering, to 3D software, slides injection and even mechanical setups.","links":""},
    //     {"name":"life sciences","category":"data viz","date_start":"1/1/2001","date_mode":"6/1/2014","date_end":"9/1/2015","level":"5","description_length":"356","description":"Life sciences, and more particularly proteomics, is a domain where I pushed visualization the furthest. The problem are intrinsically complex, with vast amount of data and were a clear truth seldomly emerge. Collaboration with world class researchers and talentuous software developers allowed innovation on the technical side, but also on the methodology.","links":"http://research-pub.gene.com/fishtones-js/howto/;fishTones.js\nhttp://research-pub.gene.com/pviz/examples/;pViz.js"},
    //     {"name":"graph","category":"data viz","date_start":"6/1/2012","date_mode":"6/1/2014","date_end":"9/1/2015","level":"4","description_length":"162","description":"If graph are everywhere, there are often complex for the human brain to apprehend. Either rich or voluminuous, different techniques are to be used for navigating.","links":"http://alexandre-masselot.blogspot.com/2014/05/viewing-graphs-in-browser.html;Viewing graphs in the browser"},
    //     {"name":"GIS/Real Time","category":"data viz","date_start":"9/1/2015","date_mode":"10/30/2018","date_end":"10/30/2018","level":"2","description_length":"230","description":"Map are a natural ground for visualization and JavaScript offers wide possibilities in the browser. I covered various aspects, from researchers collaborations, to archeological information and real time mapping of public transport","links":""},
    //     {"name":"Reproducible science","category":"data viz","date_start":"9/1/2015","date_mode":"10/30/2018","date_end":"10/30/2018","level":"","description_length":"0","description":"","links":""},
    //     {"name":"software craftsmanship","category":"data viz","date_start":"6/1/2012","date_mode":"11/1/2014","date_end":"10/30/2018","level":"5","description_length":"77","description":"everywhere in code, but must be reinvented in data viz. Special testing etc..","links":""},
    //     {"name":"CI/CD","category":"domain","date_start":"1/1/2010","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"33","description":"Hudson/Jenkins, Gitlab CI, Perl, ","links":""},
    //     {"name":"History","category":"domain","date_start":"1/1/2015","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"53","description":"Learn from history Amundsen, Brinton, turing, testing","links":""},
    //     {"name":"classification models","category":"domain","date_start":"6/1/2000","date_mode":"6/6/2003","date_end":"6/1/2005","level":"4","description_length":"23","description":"SVM ... adapt the dates","links":""},
    //     {"name":"graph","category":"domain","date_start":"6/1/2012","date_mode":"6/1/2014","date_end":"3/1/2018","level":"3","description_length":"36","description":"neo4j, analysis, Cytoscape, Gephi...","links":""},
    //     {"name":"High Performance Computing","category":"domain","date_start":"9/1/1992","date_mode":"3/1/2000","date_end":"3/1/2003","level":"5","description_length":"47","description":"many type of machines, up to 1600 nodes cluster","links":""},
    //     {"name":"NoSQL","category":"domain","date_start":"6/1/2012","date_mode":"12/1/2016","date_end":"10/30/2018","level":"3","description_length":"23","description":"MongoDB, Elastic, Neo4j","links":""},
    //     {"name":"numerical models","category":"domain","date_start":"9/1/1992","date_mode":"3/1/2000","date_end":"3/1/2000","level":"4","description_length":"62","description":"fluid dynamics, theoretical physics models, ecology and social","links":""},
    //     {"name":"text indexing/searching","category":"domain","date_start":"1/1/2010","date_mode":"3/1/2012","date_end":"3/1/2012","level":"3","description_length":"0","description":"","links":""},
    //     {"name":"Management & strategy","category":"management/lead","date_start":"5/1/2003","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"54","description":"team lead, architect and CTO @ GeneBio, senior at OCTO","links":""},
    //     {"name":"psychology","category":"management/lead","date_start":"1/1/2010","date_mode":"10/30/2018","date_end":"10/30/2018","level":"","description_length":"62","description":"individual triggers for action. customer experienece. ACN shit","links":""},
    //     {"name":"Mountain Troop instructor","category":"management/lead","date_start":"11/1/1996","date_mode":"","date_end":"10/1/1997","level":"3","description_length":"0","description":"","links":""},
    //     {"name":"Technology evangelist","category":"management/lead","date_start":"5/1/2012","date_mode":"11/1/2014","date_end":"9/1/2015","level":"4","description_length":"17","description":"Genentech-vitalit","links":""},
    //     {"name":"Unsupported polar ski expeditions","category":"management/lead","date_start":"1/1/1995","date_mode":"5/1/2002","date_end":"4/1/2004","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Agile/lean in research","category":"methodo","date_start":"6/1/2010","date_mode":"11/1/2014","date_end":"6/1/2015","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"DevOps","category":"methodo","date_start":"9/1/2016","date_mode":"10/30/2018","date_end":"10/30/2018","level":"3","description_length":"0","description":"","links":""},
    //     {"name":"Lean","category":"methodo","date_start":"9/1/2015","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Lean UX","category":"methodo","date_start":"1/1/2010","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Product Innovation","category":"methodo","date_start":"9/1/2016","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Product Ownership","category":"methodo","date_start":"9/1/2015","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"15","description":"Coaching & role","links":""},
    //     {"name":"testing","category":"methodo","date_start":"6/1/2012","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"academic assistant","category":"teaching","date_start":"3/1/1995","date_mode":"","date_end":"3/1/2000","level":"3","description_length":"0","description":"","links":""},
    //     {"name":"Introduction to data science for biologists","category":"teaching","date_start":"2/1/2005","date_mode":"","date_end":"6/1/2011","level":"3","description_length":"59","description":"1 semester per year, master student at University of Geneva","links":""},
    //     {"name":"Master in business Innovation (Crea)","category":"teaching","date_start":"10/15/2017","date_mode":"","date_end":"10/30/2018","level":"4","description_length":"61","description":"Contribute setup the cursus and teach the lean startup module","links":""},
    //     {"name":"Ph.D. & Master mentoring","category":"teaching","date_start":"9/1/2006","date_mode":"6/1/2011","date_end":"6/1/2011","level":"3","description_length":"45","description":"3 Ph.D. and various masters in bioinformatics","links":""},
    //     {"name":"Akka","category":"techno","date_start":"6/1/2012","date_mode":"6/1/2015","date_end":"10/30/2018","level":"3","description_length":"0","description":"","links":""},
    //     {"name":"C++","category":"techno","date_start":"3/1/2000","date_mode":"1/1/2008","date_end":"1/1/2010","level":"2","description_length":"0","description":"","links":""},
    //     {"name":"d3.js","category":"techno","date_start":"6/1/2013","date_mode":"6/1/2014","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Docker ecosystem","category":"techno","date_start":"12/15/2016","date_mode":"10/30/2018","date_end":"10/30/2018","level":"4","description_length":"23","description":"Docker, kubernetes, AWS","links":""},
    //     {"name":"Fortran","category":"techno","date_start":"9/1/1992","date_mode":"9/1/1999","date_end":"3/1/2000","level":"5","description_length":"0","description":"","links":""},
    //     {"name":"JavaScript & derived","category":"techno","date_start":"1/1/2000","date_mode":"1/1/2017","date_end":"10/30/2018","level":"4","description_length":"96","description":"JavaScript, Typescript, Angular(JS), React (native) and older techno (Backbone, jQuery). backend","links":""},
    //     {"name":"JVM languages","category":"techno","date_start":"1/1/2005","date_mode":"6/1/2015","date_end":"10/30/2018","level":"3","description_length":"20","description":"Java, Groovy, Kotlin","links":""},
    //     {"name":"MPI","category":"techno","date_start":"9/1/1992","date_mode":"3/1/2000","date_end":"3/1/2005","level":"5","description_length":"0","description":"","links":""},
    //     {"name":"Perl","category":"techno","date_start":"3/1/2000","date_mode":"6/1/2011","date_end":"10/30/2018","level":"4","description_length":"58","description":"clena, CI, Larry Wall... And I live with a cat named Perl.","links":"http://bit.ly/cicd-perl;Continuous Deployment in Perl: Code & Folks"},
    //     {"name":"R","category":"techno","date_start":"1/1/2008","date_mode":"6/1/2015","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"graphs","category":"techno","date_start":"1/1/2008","date_mode":"6/1/2015","date_end":"10/30/2018","level":"4","description_length":"15","description":"noe4j, jung ...","links":""},
    //     {"name":"Scala","category":"techno","date_start":"6/1/2012","date_mode":"12/1/2016","date_end":"10/30/2018","level":"4","description_length":"0","description":"","links":""},
    //     {"name":"Spark","category":"techno","date_start":"6/1/2012","date_mode":"6/1/2015","date_end":"10/30/2018","level":"2","description_length":"0","description":"","links":""}
    // ]

;