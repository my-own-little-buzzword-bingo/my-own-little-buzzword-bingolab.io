/**
 * render
 * @param container
 * @param packedSkill
 * @constructor
 */
function PackedSkillsRenderer() {
};


function elementDimensions(el) {
    return {
        width: parseFloat(d3.select(el).style('width')),
        height: parseFloat(d3.select(el).style('height'))
    }
}

/**
 * display the skills, presented through IntervalPacking
 * @param container
 * @param packedSkill
 * @param options
 */
PackedSkillsRenderer.prototype.displaySkills = function (container, packedSkill, options) {
    options = {style: {}, ...options};

    const containerDims = elementDimensions(container);
    const containerWidth = options.width || containerDims.width;
    const containerHeight = options.height || containerDims.height;

    const minDate = options.minDate || packedSkill.min();
    const maxDate = options.maxDate || packedSkill.max();

    const detailWidget = options.detailWidget;

    const scaleX = d3.scaleTime()
        .domain([
            minDate,
            maxDate
        ])
        .range([1, containerWidth - 1]);

    const scaleY = d3.scaleLinear()
        .domain([0, packedSkill.nbLanes()])
        .range([0, containerHeight - 1]);

    const color = options.style.fill || 'orange';

    const elContainer = d3.select(container);
    const elGSkills = elContainer.selectAll('g.skill')
        .data(packedSkill.list())
        .enter()
        .append('g')
        .classed('skill', true);

    if (detailWidget) {
        elGSkills.on('mouseover', function (skill) {
            detailWidget.show(
                d3.select(this),
                skill
            );
            detailedSource = skill;
        });
    }

    elGSkills.append('rect')
        .classed('distribution', true)
        .attr('x', (skill) => scaleX(skill.date_start))
        .attr('y', (skill) => scaleY(skill[IntervalPacking.tagName].lane + 1) - (scaleY(0.3) * skill.level / 5))
        .attr('height', (skill) => scaleY(0.3) * skill.level / 5)
        .attr('width', (skill) => scaleX(skill.date_end) - scaleX(skill.date_start))
        .style('fill', color)
        .style('opacity', (skill) => 1 * skill.interest / 5)
    //.style('opacity', (skill) => 0.2 + 0.8 * (skill.level / 5));

    const fontSize = scaleY(0.44)+'px';
    elGSkills.append('text')
        .classed('name', true)
        .style('font-size', fontSize)
        .attr('x', (skill) => scaleX(skill.date_start))
        .attr('y', (skill) => scaleY(skill[IntervalPacking.tagName].lane+1)-scaleY(0.3) * skill.level / 5 - 2 )
        .text((skill) => skill.name)
};

PackedSkillsRenderer.prototype.displayCategoryTitle = function (container, textList, options) {
    options = {style: {}, ...options};

    const containerDims = elementDimensions(container);

    const containerWidth = options.width || containerDims.width;
    const containerHeight = options.height || containerDims.height;

    const elContainer = d3.select(container);
    elContainer.append('g')
        .classed('category', true)
        .attr('transform', `translate(${containerWidth / 2}, ${containerHeight / 2}) rotate(-90)`)
        .append('text')
        .classed('title', true)
        .selectAll('tspan')
        .data(textList)
        .enter()
        .append('tspan')
        .attr('x', 0)
        .attr('dy', (t, i) => i * 12)
        .text((t) => t.toUpperCase());

};

/**
 * display a list of categories in a vertical text in boxes
 * @param container a d3 selectable containers
 * @param categories a lit of categories (main attribute shall be 'display')
 * @param dimensions a lit of the same size as categories, with at least a 'height' attribute
 * @param options
 */
PackedSkillsRenderer.prototype.displayCategories = function (container, categories, dimensions, options) {
    const self = this;
    options = {width: 30, ...options};

    const el = d3.select(container);

    // mid-deep copy the categories list into a new list and add height/offset attributes
    let offset = 0;
    const dCat = _.map(categories, (c, i) => {
        const r = {...c};
        r.textList = c.display.split('\n');
        r.height = dimensions[i].height;
        r.offset = (dimensions[i].offset === undefined) ? offset : dimensions[i].offset;
        offset += r.height;
        return r;
    });
    // add <g> element shifted by the vertical offset
    const gCat = el.selectAll('g.category-group')
        .data(dCat)
        .enter()
        .append('g')
        .classed('category-group', true)
        .attr('transform', (c) => `translate(0, ${c.offset})`);

    // render the category content
    el.selectAll('g.category-group')
        .each(function (c) {
            self.displayCategoryTitle(this, c.textList, {width: options.width, height: c.height});
        });

    // find the maximal font size where the text enter the category height
    // beware of multiple lines
    let minFont = 100000;
    el.selectAll('g.category-group')
        .selectAll('text.title')
        .each(function (c) {
            d3.select(this).selectAll('tspan')
                .each(function (t) {
                    const fontSize = 15 * c.height / this.getComputedTextLength();
                    minFont = Math.min(minFont, fontSize);
                });
        });
    const minFontHeight = 0.9 * options.width / _.chain(dCat).map((c) => c.textList.length).max().value();
    minFont = Math.min(minFont, minFontHeight);

    // change the font size, vertical position and line spacing
    el.selectAll('g.category-group')
        .selectAll('text.title')
        .attr('y', (c) => -(c.textList.length - 1) / 2 * minFont) // align he text vertically (i.e. on the horizontal axis, as it's turned 90°
        .style('font-size', minFont+'px') // font size
        .selectAll('tspan')
        .attr('dy', (t, i) => i && minFont); // line position

    //display a colored line on the right
    gCat
        .append('line')
        .classed('marker', true)
        .attr('x1', options.width)
        .attr('x2', options.width)
        .attr('y1', 0)
        .attr('y2', (c) => c.height)
        .style('stroke', (c) => c.color)
        .style('stroke-width', Math.min(options.width / 15, 4))

};

PackedSkillsRenderer.prototype.displayPositions = function (container, positions, options) {
    options = {height: 50, ...options};
    const containerDims = elementDimensions(container);
    const containerWidth = options.width || containerDims.width;
    const containerHeight = options.height;

    const detailWidget = options.detailWidget;

    const el = d3.select(container);

    const scaleX = d3.scaleTime()
        .domain([
            options.minDate || _.chain(positions).map('date_start').min().value(),
            options.maxDate || _.chain(positions).map('date_end').max().value(),
        ])
        .range([0, containerWidth]);

    const elGroups = el.selectAll('g.position')
        .data(positions)
        .enter()
        .append('g')
        .classed('position', true);

    // draw the boxes
    const wSideTriangle = containerHeight / 8;
    elGroups
        .filter((p) => !p.is_failure)
        .append('path')
        .classed('box', true)
        .attr('d', (p) => {
            const x0 = scaleX(p.date_start);
            const x1 = scaleX(p.date_end);
            const wLeft = p.is_started_before ? 0 : wSideTriangle;
            const wRight = p.is_on_going ? 0 : wSideTriangle;
            const path = `M ${x0} ${containerHeight / 2} ` +
                `L ${x0 + wLeft} ${containerHeight} ` +
                `L ${x1 - wRight} ${containerHeight} ` +
                `L ${x1} ${containerHeight / 2}  ` +
                `L ${x1 - wRight} 0 ` +
                `L ${x0 + wLeft} 0 ` +
                'Z';
            return path;
        });

    //circles + rectangle for failure position
    const radiusFailure = containerHeight / 7;
    elGroups
        .filter((p) => p.is_failure)
        .append('rect')
        .classed('transparent', true)
        .attr('x', (p) => scaleX(p.date_start))
        .attr('y', 0)
        .attr('height', containerHeight)
        .attr('width', (p) => scaleX(p.date_end) - scaleX(p.date_start));
    elGroups
        .filter((p) => p.is_failure)
        .append('circle')
        .classed('failure', true)
        .attr('cx', (p) => scaleX(p.date_start) + radiusFailure)
        .attr('cy', containerHeight / 2)
        .attr('r', radiusFailure);
    elGroups
        .filter((p) => p.is_failure)
        .append('circle')
        .classed('failure', true)
        .attr('cx', (p) => scaleX(p.date_end) - radiusFailure)
        .attr('cy', containerHeight / 2)
        .attr('r', radiusFailure)
    elGroups
        .filter((p) => p.is_failure)
        .append('rect')
        .classed('failure', true)
        .attr('x', (p) => scaleX(p.date_start) + radiusFailure)
        .attr('y', containerHeight / 2 - radiusFailure)
        .attr('width', (p) => Math.max(0, scaleX(p.date_end) - scaleX(p.date_start) - 2 * radiusFailure))
        .attr('height', 2 * radiusFailure);

    //add text
    const elTexts = elGroups
        .filter((p) => !p.is_failure)
        .append('g')
        .attr('transform', (p) => `translate(${(scaleX(p.date_start) + scaleX(p.date_end)) / 2}, 0)`);

    elTexts.append('text')
        .classed('organization', true)
        .text((p) => p.name)
        .attr('y', 5);
    elTexts.append('text')
        .classed('location', true)
        .text((p) => p.location)
        .attr('y', containerHeight / 2);
    elTexts.append('text')
        .classed('role', true)
        .text((p) => `${p.role}`)
        .attr('y', containerHeight - 6);

    //adapt font size;
    let minRatio = 100000;
    elGroups.selectAll('text')
        .each(function (p) {
            const w = scaleX(p.date_end) - scaleX(p.date_start) - 2 * radiusFailure;
            ratio = w / this.getComputedTextLength();
            minRatio = Math.min(minRatio, ratio);
        });
    minRatio = Math.min(containerHeight / 60, minRatio);
    elGroups.style('font-size', (minRatio * 100) + '%');

    if (detailWidget) {
        elGroups.on('mouseover', function (skill) {
            if (detailedSource === skill && detailWidget._isVisible) {
                return;
            }
            detailWidget.hide();
            detailWidget.show(
                d3.select(this),
                skill
            );
            detailedSource = skill;
        });
    }
};

PackedSkillsRenderer.prototype.displayXAxis = function (container, minDate, maxDate, options) {
    options = {...options};
    const containerDims = elementDimensions(container);

    const containerWidth = options.width || containerDims.width;
    const containerHeight = options.height || containerDims.height;
    const axisHeight = options.axisHeight || containerHeight * 0.2;

    const el = d3.select(container);

    const scaleX = d3.scaleTime()
        .domain([
            minDate,
            maxDate
        ])
        .range([1, containerWidth - 1]);

    const xAxis = d3.axisBottom(scaleX)
        .ticks(5)
        .tickSize(containerHeight - axisHeight);

    el.append('g')
        .classed('axis-date', true)
        .attr('transform', `translate(0,0)`)
        .call(xAxis);
    el.selectAll('g.axis-date')
        .selectAll('text')
        .style('font-size', `${axisHeight - 2}px`)
    el.selectAll('g.axis-date')
        .selectAll('line')
        .style('stroke-width', `${Math.min(3, 0.5 + 3 * containerWidth / 1000)}px`)

};

/**
 * display the details for a skill, with a free text, eventual links and so on
 * @constructor
 */
function ModalDetails(elContainer, options) {
    this.elContainer = elContainer;
    const node = this.elContainer.node();
    //const containerDims = elementDimensions(elContainer);

    this.containerWidth = node.clientWidth;
    this.containerHeight = node.clientHeight;

    this.options = {
        ...options
    };
    this.height = this.options.height || Math.min(0.8 * this.containerHeight, Math.max(200, this.containerHeight * 0.25));
    this.width = this.options.width || Math.min(0.8 * this.containerWidth, Math.max(300, this.containerWidth * 0.25));
    this.margin = (this.options.margin === undefined) ? 10 : options.margin;
    this.padding = (this.options.padding === undefined) ? 3 : options.padding;

    this.layout =
        {
            title: {
                height: Math.max(this.height / 4, 30) - 2 * this.padding,
                width: this.width - 2 * this.padding,
                offset: {x: this.padding, y: this.padding}
            },
        };

    this.layout.description = {
        height: Math.max(this.height / 2, 30) - 2 * this.padding,
        width: this.width - 2 * this.padding,
        offset: {x: this.padding, y: this.layout.title.height + 3 * this.padding}
    };

    this.layout.links = {
        height: this.height - 6 * this.padding - this.layout.title.height - this.layout.description.height,
        width: this.width - 2 * this.padding,
        offset: {x: this.padding, y: this.layout.title.height + this.layout.description.height + 5 * this.padding}
    };

}

ModalDetails.prototype.hide = function () {
    this._source = undefined;
    if (!this._elForeignObject || !this._isVisible) {
        return;
    }
    this._elForeignObject
        .data([])
        .exit()
        .remove();
    this._isVisible = false;
};

ModalDetails.prototype.show = function (elTarget, skill) {
    const self = this;

    if (skill === this._source) {
        return;
    }
    if (this._source) {
        this.hide();
    }

    self._isVisible = true;
    this._source = skill;

    const xyPosition = (foNode) => {

        foNode.getBoundingClientRect();
        const bbox = d3.select(foNode).selectAll('div.skill-details').node().getBoundingClientRect();

        const bboxTarget = elTarget.node().getBoundingClientRect();
        const bboxContainer = this.elContainer.node().getBoundingClientRect();

        const xMidTarget = -bboxContainer.x + bboxTarget.x + bboxTarget.width / 2;
        const yMidTarget = -bboxContainer.y + bboxTarget.y + bboxTarget.height / 2;
        const downwards = (yMidTarget <= self.containerHeight / 2);
        const yMid = downwards ?
            (yMidTarget + bboxTarget.height / 2 + 2 + bbox.height / 2) :
            (yMidTarget - bboxTarget.height / 2 - 2 - bbox.height / 2);

        const xMid = Math.min(
            Math.max(self.margin + bbox.width / 2, xMidTarget),
            self.containerWidth - self.margin - bbox.width / 2,
        );

        return {x: xMid - self.width / 2, y: yMid - bbox.height / 2};
    };

    const elForeignObject = this.elContainer.selectAll('foreignObject.skill-details')
        .data([])
        .data([skill])
        .enter()
        .append('foreignObject')
        .classed('skill-details', true)
        .attr('width', self.width)
        .on('mouseleave', function () {
            self.hide();
        });
    self._elForeignObject = elForeignObject;


    const elMain = elForeignObject.append('xhtml:body')
        .append('div')
        .classed('skill-details', true);

    elMain.append('div')
        .classed('title', true)
        .style('padding', self.padding + 'px')
        .append('span')
        .text((s) => s.name);

    elMain.append('div')
        .classed('description', true)
        .selectAll('p')
        .data((d) => d.description.split('\n'))
        .enter()
        .append('p')
        .html((s) => s);

    if (skill.links) {
        elMain.append('div')
            .classed('links', true)
            .append('ul')
            .selectAll('li.link')
            .data((skill.links))
            .enter()
            .append('li')
            .classed('link', true)
            .append('a')
            .attr('href', (l) => l.url)
            .attr('target', '__MOLB_LINK__')
            .text((l) => l.title);
    }


    // once the div is filled with the textual content, its size is know and we can position it rightly
    elForeignObject
        .attr('x', function () {
            return xyPosition(this).x
        })
        .attr('y', function () {
            return xyPosition(this).y
        })

};
