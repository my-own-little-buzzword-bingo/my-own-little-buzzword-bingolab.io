Promise.all([
    d3.csv('src/data/cv topic - skills.csv'),
    d3.csv('src/data/cv topic - timeline.csv')
])
    .then((cvData) => {
        const [skillsRaw, positions] = cvData;
        const tl = new SkillTimelinetBuilder().build(skillsRaw, skillsMeta);


        const packer = new IntervalPacker(
            (s) => s.date_start.valueOf(),
            (s) => s.date_end.valueOf(),
        );

        const packedSkills = _.chain(tl)
            .map((skills) => packer.pack(skills, 'topFirst'))
            .sortBy((packing) => packing.list()[0].metadata.rank)
            .map((packing) => {
                return {
                    metadata: packing.list()[0].metadata,
                    packing: packing
                }
            })
            .value();

        const target = document.getElementById('timeline');
        const width = parseFloat(d3.select(target).style('width'));
        const height = parseFloat(d3.select(target).style('height'));

        const heightAxis = Math.min(30, 0.08 * height);
        const heightPositions = Math.min(50, 0.15 * height);
        const heightLanes = height - heightAxis - heightPositions;
        const heightDeltaCategories = 3;
        const heightForLanes = heightLanes - (packedSkills.length - 1) * heightDeltaCategories
        const widthCategory = Math.min(70, 0.1 * width);
        const widthLanes = width - widthCategory;

        let offset = 0;
        const totLanes = _.chain(packedSkills).map((ps) => ps.packing.nbLanes()).sum().value();
        _.each(packedSkills, (ps) => {
            ps.metadata.offset = offset;
            ps.metadata.height = heightForLanes * ps.packing.nbLanes() / totLanes;

            offset += ps.metadata.height + heightDeltaCategories;
        });

        const minDate = _.chain(packedSkills).map((ps) => ps.packing.min()).min().value();
        const maxDate = _.chain(packedSkills).map((ps) => ps.packing.max()).max().value();
        const renderer = new PackedSkillsRenderer();
        const elMain = d3.select(target);

        const skillDetailWidget = new ModalDetails(
            elMain,
            {width: Math.min(width * 0.8, 400)}
        );

        const gAxis = elMain
            .append('g')
            .classed('axis', true)
            .attr('transform', (ps) => `translate(${widthCategory}, 0)`);

        renderer.displayXAxis(gAxis.node(), minDate, maxDate,
            {
                axisHeight: heightAxis,
                height: height,
                width: widthLanes
            }
        );

        const gCategorySkills = elMain.selectAll('g.category-skills')
            .data(packedSkills)
            .enter()
            .append('g')
            .classed('category-skills', true)
            .attr('transform', (ps) => `translate(0, ${ps.metadata.offset})`);

        gCategorySkills.append('g')
            .classed('skills', true)
            .attr('transform', `translate(${widthCategory},0)`)
            .each(function (ps) {
                renderer.displaySkills(
                    this,
                    ps.packing,
                    {
                        height: ps.metadata.height,
                        width: widthLanes,
                        minDate: minDate,
                        maxDate: maxDate,
                        style: {fill: ps.metadata.color},
                        detailWidget: skillDetailWidget
                    }
                );
            });

        const gCategoryTitles = elMain.append('g')
            .classed('categories', true);
        renderer.displayCategories(
            gCategoryTitles.node(),
            _.map(packedSkills, 'metadata'),
            _.map(packedSkills, (ps) => {
                return {
                    height: ps.metadata.height,
                    offset: ps.metadata.offset
                }
            })
        );

        const gPositions = elMain.append('g')
            .classed('positions', true)
            .attr('transform', `translate(${widthCategory}, ${heightLanes + 2})`)

        const datedPositions = positions.map((p) => {
            p.date_end = new Date(p.date_end);
            p.date_start = new Date(p.date_start);
            return p;
        });
        renderer.displayPositions(
            gPositions.node(),
            datedPositions,
            {
                height: heightPositions - 4,
                width: widthLanes,
                minDate: minDate,
                maxDate: maxDate,
                detailWidget: skillDetailWidget
            }
        );
    })



